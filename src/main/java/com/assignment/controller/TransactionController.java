package com.assignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.controller.request.ResultMessage;
import com.assignment.controller.response.TransactionResponseList;
import com.assignment.service.TransactionService;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
@CrossOrigin
@RestController
public class TransactionController {

	@Autowired
	private TransactionService transactionService;

	/**
	 * Endpoint to add new Transaction with associated Account.
	 * 
	 * @param accountId
	 * @param amount
	 * @return ResponseEntity<ResultMessage>
	 */
	@GetMapping(value = "/transaction/add/{accountId}/{amount}")
	public ResponseEntity<ResultMessage> addTransaction(@PathVariable Long accountId, @PathVariable Long amount) {

		ResultMessage responseMessage = transactionService.addTransaction(accountId, amount);
		// ResultMessage responseMessage =
		// transactionService.addTransaction(transactionReq);
		return new ResponseEntity<>(responseMessage, HttpStatus.OK);
	}

	/**
	 * Endpoint to get Transactions associated with an Account.
	 * 
	 * @param accountId
	 * @return ResponseEntity<TransactionResponseList>
	 */
	@GetMapping(value = "/transactions/{accountId}")
	public ResponseEntity<TransactionResponseList> getTransactionsByAccountId(@PathVariable Long accountId) {
		TransactionResponseList transList = transactionService.getTransactionsByAccountId(accountId);
		return new ResponseEntity<>(transList, HttpStatus.OK);

	}

}
