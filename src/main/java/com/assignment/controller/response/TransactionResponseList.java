package com.assignment.controller.response;

import java.util.List;

public class TransactionResponseList {

	private List<TransactionResponse> transList;

	
	public List<TransactionResponse> getTransList() {
		return transList;
	}

	public void setTransList(List<TransactionResponse> transList) {
		this.transList = transList;
	}
	
	
	
	
}
