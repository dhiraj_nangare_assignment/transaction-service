package com.assignment.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.controller.request.ResultMessage;
import com.assignment.controller.response.TransactionResponse;
import com.assignment.controller.response.TransactionResponseList;
import com.assignment.dao.TransactionDao;
import com.assignment.entity.Transaction;
import com.assignment.model.ResultCode;
import com.assignment.service.TransactionService;
import com.assignment.util.Constants;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	private TransactionDao transactionDao;

	/** {@inheritDoc} */
	@Override
	public TransactionResponseList getTransactionsByAccountId(Long accountId) {
		TransactionResponseList transactionResponseList = new TransactionResponseList();
		// Get all transaction of this Account.
		List<Transaction> transactionList = transactionDao.getAllTransactions(accountId);
		// Build TransactionResponse List.
		List<TransactionResponse> transResponseList = this.buildTransactionsResponse(transactionList);
		transactionResponseList.setTransList(transResponseList);
		return transactionResponseList;
	}

	/**
	 * This is used to build TransactionResponse List.
	 * 
	 * @param transList
	 * @return
	 */
	private List<TransactionResponse> buildTransactionsResponse(List<Transaction> transList) {

		List<TransactionResponse> transResponseList = transList.stream().map(t -> {
			TransactionResponse transactionResponse = new TransactionResponse(t.getTransId(), t.getTransactionType(),
					t.getAmount(), t.getAccountId());
			return transactionResponse;
		}).collect(Collectors.toList());

		return transResponseList;
	}

	/** {@inheritDoc} */
	@Override
	public ResultMessage addTransaction(Long accountId, Long amount) {

		ResultMessage response = new ResultMessage();
		// create new Transaction
		Transaction transaction = new Transaction();
		transaction.setAccountId(accountId);
		transaction.setAmount(amount);
		transaction.setTransactionType(Constants.CURRENT_ACCOUNT);
		// Persist new Transaction into DB.
		transactionDao.createTransaction(transaction);
		// Build success response.
		this.setResponseMessage(ResultCode.SUCCESS, response);
		return response;
	}

	/**
	 * This method is used to build Response message.
	 * 
	 * @param updateResponse
	 */
	private void setResponseMessage(ResultCode resultCode, ResultMessage updateResponse) {
		updateResponse.setResultStatus(resultCode.getStatus());
		updateResponse.setResultCode(resultCode.getCode());
		updateResponse.setMessage(resultCode.getMessage());
	}

}
