package com.assignment.service;

import com.assignment.controller.request.ResultMessage;
import com.assignment.controller.response.TransactionResponseList;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
public interface TransactionService {

	/**
	 * This method is responsible to add new Transactions to the system associated
	 * with an Account..
	 * 
	 * @param accountId
	 * @param amount
	 * @return ResultMessage
	 */
	public ResultMessage addTransaction(Long accountId, Long amount);

	/**
	 * This method is responsible to get Transactions associated with an Account.
	 * 
	 * @param accountId
	 * @return
	 */
	public TransactionResponseList getTransactionsByAccountId(Long accountId);

}
