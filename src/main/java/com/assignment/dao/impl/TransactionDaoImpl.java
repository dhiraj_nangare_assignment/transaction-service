package com.assignment.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import com.assignment.dao.TransactionDao;
import com.assignment.entity.Transaction;
import com.assignment.util.Constants;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
@Transactional
@Repository
@PropertySource("classpath:sql_query.properties")
public class TransactionDaoImpl implements TransactionDao {

	@PersistenceContext
	private EntityManager entityManager;

	// Query to get all Transactions by Account Id
	@Value("${query.get.transaction.byAcountId}")
	private String queryGetAllTransactions;

	/** {@inheritDoc} */
	@Override
	public void createTransaction(Transaction newTransaction) {
		entityManager.persist(newTransaction);

	}

	/** {@inheritDoc} */
	@Override
	public List<Transaction> getAllTransactions(Long accountId) {
		return entityManager.createQuery(queryGetAllTransactions, Transaction.class)
				.setParameter(Constants.ACCOUNT_ID, accountId).getResultList();
	}

}
