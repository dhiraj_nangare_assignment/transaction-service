package com.assignment.dao;

import java.util.List;

import com.assignment.entity.Transaction;

/**
 * This is Transaction DAO class
 * 
 * @author Dhiraj.Nangare
 *
 */
public interface TransactionDao {

	/**
	 * Persist new Transaction into DB.
	 * 
	 * @param newTransaction
	 */
	public void createTransaction(Transaction newTransaction);

	/**
	 * Get All Transaction from DB.
	 * 
	 * @param accountId
	 * @return
	 */
	public List<Transaction> getAllTransactions(Long accountId);

}
