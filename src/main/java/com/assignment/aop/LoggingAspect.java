package com.assignment.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * An Aspect for logging the method entry & exit points.
 *
 * @author @author Dhiraj.Nangare
 */
@Aspect
@Component
public class LoggingAspect {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * Before Aspect for logging the method entry point.
	 *
	 * @param joinPoint
	 *            of type JoinPoint
	 */
	@Before("execution(* com.assignment..*(..))")
	public void before(JoinPoint joinPoint) {
		logger.info("{}.{}() Start", joinPoint.getSignature().getDeclaringType().getSimpleName(),
				joinPoint.getSignature().getName());
	}

	/**
	 * Before Aspect for logging the method input param details entry point.
	 *
	 * @param joinPoint
	 *            of type JoinPoint
	 */
	@Before("execution(* com.assignment.controller..*(..)) || execution(* com.assignment.service..*(..)) || execution(* com.assignment.dao..*(..))")
	public void loggingParameters(JoinPoint joinPoint) {
		StringBuilder requestParameters = new StringBuilder();
		for (Object requestParam : joinPoint.getArgs()) {
			try {
				ObjectMapper objectMapper = new ObjectMapper();
				String requestParamString = objectMapper.writeValueAsString(requestParam);
				requestParameters.append(requestParamString).append(", ");
			} catch (JsonProcessingException jsonProcessingException) {
				logger.error("Exception while logging request parameters. {}", jsonProcessingException);
			}
		}

		logger.debug("Request Parameter = {}", requestParameters);
	}

	/**
	 * Around Aspect for logging the method execution time.
	 *
	 * @param joinPoint
	 *            of type ProceedingJoinPoint
	 * @return Object
	 * @throws Throwable
	 */
	@Around("execution(* com.assignment.controller..*(..)) || execution(* com.assignment.dao..*(..))")
	public Object measureMethodExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
		long startTime = System.currentTimeMillis();
		Object output = joinPoint.proceed();
		long endTime = System.currentTimeMillis();
		logger.info("PERFORMANCE: {}.{}() - Total execution time = {} ms.",
				joinPoint.getSignature().getDeclaringType().getSimpleName(), joinPoint.getSignature().getName(),
				(endTime - startTime));
		return output;
	}

	/**
	 * After Aspect for logging the method exit point.
	 *
	 * @param joinPoint
	 *            of type JoinPoint
	 */
	@After("execution(* com.assignment..*(..))")
	public void after(JoinPoint joinPoint) {
		logger.info("{}.{}() End", joinPoint.getSignature().getDeclaringType().getSimpleName(),
				joinPoint.getSignature().getName());
	}
}
