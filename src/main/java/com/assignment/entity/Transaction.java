package com.assignment.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
@Entity
@Table(name = "transaction")
public class Transaction {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long transId;
	
	@Column(name = "transaction_type")
	private String transactionType;
	
	@Column(name = "amount")
	private Long amount;
	
	@Column(name = "account_id")
	private Long accountId;

	public Transaction() {
		
	}

	public Long getTransId() {
		return transId;
	}

	public void setTransId(Long transId) {
		this.transId = transId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	@Override
	public String toString() {
		return "Transaction [transId=" + transId + ", transactionType=" + transactionType + ", amount=" + amount
				+ ", accountId=" + accountId + "]";
	}
	
	
	
	

}
