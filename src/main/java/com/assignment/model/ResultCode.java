package com.assignment.model;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
public enum ResultCode {

    SUCCESS("0000", "success", "Success"),
    INVALID_ACCOUNT_ID("0001", "Failure", "Invalid Account Id.");
    

    private String code;
    private String status;
    private String message;

    /**
     * @param code of type String
     * @param status of type String
     * @param message of type String
     */
    private ResultCode(String code, String status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    public static ResultCode getResponseStatusByCode(final String value) {
        ResultCode responseStatusToReturn = null;
        for (ResultCode resultCode : ResultCode.values()) {
            if (value != null && resultCode.getCode().equals(value)) {
                responseStatusToReturn = resultCode;
                break;
            }
        }
        return responseStatusToReturn;
    }
}
